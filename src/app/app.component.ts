import { AuthenticationService } from './auth/authentication.service';
import { UserDetailService } from './console-application/core/user-detail.service';
import { Component, OnInit } from '@angular/core';
import { TeamDetailService } from './console-application/live-tracking/team-details/team-detail-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'fast-console';
  public isAuthorized: boolean;
  constructor(
    private soburdinateService: TeamDetailService,
    private supervisorService: UserDetailService,
    private auth: AuthenticationService
    ) {
    // this.getAuth();
    this.soburdinateService.getSupervisorSubordinates('P6127');
    this.supervisorService.getSupervisorDetails('P6127');
  }

  getAuth() {
    console.log('here');
    let auth;
    auth = this.auth.isAuthorized();
    if (auth === 'not authorized and not tracking') {
      this.isAuthorized = false;
    } else {
      this.isAuthorized = true;
    }
  }
  ngOnInit() {
  }
}
