import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError, merge } from 'rxjs/operators';
import { Router } from '@angular/router';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
};

const param = {
  'staffId': '',
  'platform': ''
};


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  staffId: Subject<any> =  new BehaviorSubject<any>(null);
  staffDetails = new BehaviorSubject<any>(null);
  staffDetails$ = this.staffDetails.asObservable();
  
  public Authorized: boolean;
  constructor( private http: HttpClient, private route: Router) { }

  getStaffRole(): Observable<any> {
    return this.http.get(`${environment.fastconsole_getStaffRole}?StaffId=P6127`)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  // get isTracking() {
  //   let isTracking: boolean;
  //   this.getStaffRole().subscribe(res => {
  //     if (res.code = '00') {
  //       isTracking = res.data.hasTrackableTeam;
  //     }
  //   });
  //   return isTracking;
  // }

  isAuthorized() {
    let authorized;
    let hasTracking;
    authorized = JSON.parse(sessionStorage.auth);
    authorized = authorized.authorize;
    hasTracking = JSON.parse(sessionStorage.auth);
    hasTracking = hasTracking.hasTrackableTeam;
    if (hasTracking ) {
      return 'authorized and isTracking';
    } else if ( authorized === true && hasTracking === false ) {
      return 'authorized and notTracking';
    } else if (authorized === false && hasTracking === false ) {
      return 'not authorized and not tracking';
    }
  }

  getAuthorization() {
    return  new Promise((resolve, reject) => {
      this.getStaffRole().subscribe(res => {
      if (res.code === '00') {
        if ( !sessionStorage.getItem('auth')) {
          sessionStorage.setItem('auth', JSON.stringify(res.data));
        } else {
          sessionStorage.clear();
          sessionStorage.setItem('auth', JSON.stringify(res.data));
        }
        // this.route.navigateByUrl('/live-map');
        resolve(res);
      } else {

      }
    },
    err => {
      reject(err);
    });
    });
  }

  _getStaffDetails (): Observable<any> {
    return this.http.post(environment.galaxy_getUserDetails, param)
    .pipe(
      retry(3), catchError(this.handleError)
    );
  }

  getStaffDetails() {
    this._getStaffDetails().subscribe( details => {
      if ( details.ResponseCode === '00') {
        this.staffDetails.next(details.ResponseObject[0]);
      }
    });
  }

  handleError( error: HttpErrorResponse ) {
    return throwError(error);
  }

}