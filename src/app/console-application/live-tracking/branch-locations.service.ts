import { Injectable } from '@angular/core';
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BranchLocationsService {
  branchLocationCollection: AngularFirestoreDocument<any[]>;
  branchlocations: Observable<any[]>;
  branchCoords: Subject<any[]> = new BehaviorSubject<any[]>([]);
  branchNames: Subject<any[]> = new BehaviorSubject<any[]>([]);


  constructor(public firestore: AngularFirestore) { }

  fetchBranches() {
    console.log('entered branch service');
    this.branchLocationCollection = this.firestore.doc('branch-locations/9CJMqthgw19Ynfx7RiFw');
    this.branchlocations = this.branchLocationCollection.valueChanges();
  }

  getBranches() {
    this.branchlocations.subscribe(location => {

      const branchCoords = Object.values(location);
      this.branchCoords.next(branchCoords);
      const branchNames = Object.keys(location);
      this.branchNames.next(branchNames);
    });
  }
}

export class BranchCoords {
  lat: number;
  long: number;
}
