import { TestBed } from '@angular/core/testing';

import { BranchLocationsService } from './branch-locations.service';

describe('BranchLocationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BranchLocationsService = TestBed.get(BranchLocationsService);
    expect(service).toBeTruthy();
  });
});
