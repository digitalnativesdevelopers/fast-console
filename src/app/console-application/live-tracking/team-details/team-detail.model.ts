export class TeamMember implements TeamDetail {
  position: number;
  name: string;
  staffId: string;
  email: string;
  phone: number;
  supervisor: string;
  constructor(
    position: number,
    name: string,
    staffId: string,
    email: string,
    phone: number,
    supervisor: string) {
      this.position = position;
      this.name = name,
      this.staffId = staffId;
      this.email = email;
      this.phone = phone;
      this.supervisor = supervisor;
  }
}

export interface TeamDetail {
  position: number;
  name: string;
  staffId: string;
  email: string;
  phone: number;
  supervisor: string;
}
