import { TeamDetailService } from './team-detail-service.service';
import { TeamDetail, TeamMember } from './team-detail.model';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ProfileMapService } from '../staff-page/profile-page/profile-map.service';


const TEAM_DATA: TeamDetail[] = [
  {
    position: 1,
    name: 'Oluwafemi Emmanuel',
    staffId: 'P7560',
    email: 'emmanuel.oluwafemi@fidelitybank.ng',
    phone: 0o7034232340,
    supervisor: 'Adetola Fadiya'
  },
  {
    position: 2,
    name: 'Sulaiman Olaosebikan',
    staffId: 'P7561',
    email: 'sulaiman.olaosebikan@fidelitybank.ng',
    phone: 0o7034232340,
    supervisor: 'Adetola Fadiya'
  },
  {
    position: 3,
    name: 'Simpa Yekini',
    staffId: 'P7566',
    email: 'simpa.yekini@fidelitybank.ng',
    phone: 0o7034232340,
    supervisor: 'Adetola Fadiya'
  }
];


@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})


export class TeamDetailsComponent implements OnInit {

  dateSearch: FormGroup;

  teamMembers: TeamMember[];
  // filterdTeamMembers: TeamDetail[];
  displayedColumns: string[] = ['position', 'name', 'staffid', 'email', 'phone', 'supervisor'];
  dataSource: any; // MatTableDataSource<TeamMember[]>; // new MatTableDataSource(this.teamMembers);

  minDate = new Date(2019, 0, 1);
  maxDate = new Date(2020, 0, 1);


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(private teamMembersService: TeamDetailService,
    private profileMapService: ProfileMapService) {
    this.dateSearch = this.createFormGroup();
   }

  ngOnInit() {
    this.teamMembersService.teamMembers.subscribe((teamMembers) => {
      this.teamMembers = teamMembers;
      this.dataSource =  new MatTableDataSource(this.teamMembers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.profileMapService.fetchStaffLocation();
  }

  createFormGroup() {
    return new FormGroup({
      startDate: new FormControl('', Validators.required),
      endDate: new FormControl('', Validators.required)
    });
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
