import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable, from } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { TeamMember } from './team-detail.model';
@Injectable({
  providedIn: 'root'
})
export class TeamDetailService {
  teamMembers: Subject<TeamMember[]> = new BehaviorSubject<TeamMember[]>([]);

  constructor(private http: HttpClient) { }

  getSupervisorSubordinatesFromService(staffId: string): Observable<any> {
    const path = environment.fastconsole_getSupervisorSubordinates;
    return this.http
      .get<any>(path, {
        params: {
          staffId: staffId
        }
      })
      .pipe();
  }

  getSupervisorSubordinates(staffId: string) {
    this.getSupervisorSubordinatesFromService(staffId).subscribe((response) => {
      console.log(response);
      const teamMembers = this.extractTeamData(response);

      this.teamMembers.next(teamMembers);
    });
  }

  extractTeamData(response: any): TeamMember[] {
    const teamMembers = [];
    if (response.code === '00') {
      let memberCount = 0;
      for (const member of response.data) {
        memberCount++;

        const teamMember = new TeamMember(
          memberCount,
          member.staffName,
          member.staffId,
          member.email,
          member.mobilePhone,
          member.directLineManager);
        teamMembers.push(teamMember);
      }
    }
    return teamMembers;
  }
}
