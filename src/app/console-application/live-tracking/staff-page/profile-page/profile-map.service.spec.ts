import { TestBed } from '@angular/core/testing';

import { ProfileMapService } from './profile-map.service';

describe('ProfileMapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfileMapService = TestBed.get(ProfileMapService);
    expect(service).toBeTruthy();
  });
});
