import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { MapModel } from '../../map-model';
import { map } from 'rxjs/operators';
import { ProfilePageComponent } from './profile-page.component';
// import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ProfileMapService {
  staffLocationCollection: AngularFirestoreCollection<MapModel[]>;
  staffLocationDoc: AngularFirestoreDocument<MapModel[]>;
  stafflocations: Observable<MapModel[]>;

  staffLastLocation: Subject<MapModel[]> = new BehaviorSubject<MapModel[]>([]);
  dateTime: Subject<any> = new BehaviorSubject<any>('');

  allOfOneStaffLocation: Subject<any> = new BehaviorSubject<any>('');



  constructor(public firestore: AngularFirestore) { }

  fetchStaffLocation() {
    return new Promise((resolve, reject) => {
      this.staffLocationCollection = this.firestore.collection('live-locations');
      this.stafflocations = this.staffLocationCollection.snapshotChanges().pipe(

        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as MapModel;
          data.id = a.payload.doc.id;
          return data;
        }))
      );
      if (this.stafflocations) {
        resolve(this.stafflocations);
      } else {
        reject(this.stafflocations);
      }
    });

  }

  getStaffLocations(hashedStaffId) {
    return new Promise((resolve, reject) => {
      let result: any;
      this.fetchStaffLocation().then(response => {

        this.stafflocations.subscribe(location => {
          result = location.filter(userId => userId.id === hashedStaffId);
          if (result) {
            this.allOfOneStaffLocation.next(result[0]);

            const resultSize = Object.values(result[0]).length;

            const dateTime = Object.keys(result[0]).slice(resultSize - 2, resultSize - 1);
            const staffLastLocation = Object.values(result[0]).slice(resultSize - 2, resultSize - 1);
            this.staffLastLocation.next(staffLastLocation);
            this.dateTime.next(dateTime);
            resolve(staffLastLocation);
          } else {
            reject(null);
          }
        });
      },
        (error) => {
        });

    });

  }

}
