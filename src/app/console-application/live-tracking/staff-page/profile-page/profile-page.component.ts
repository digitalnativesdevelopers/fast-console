import { Component, OnInit, NgZone } from '@angular/core';
import { ProfileMapService } from './profile-map.service';
import { User } from 'src/app/console-application/core/user-detail.model';
import { UserDetailService } from 'src/app/console-application/core/user-detail.service';
import { MapModel } from '../../map-model';
import { MapsAPILoader } from '@agm/core';

declare var google: any;
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { StaffService } from '../staff.service';
import * as moment from 'moment';
import { extendMoment } from 'moment-range';
import { element } from '@angular/core/src/render3';
import { BranchLocationsService, BranchCoords } from '../../branch-locations.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  trackHistoryCoords: TrackHistoryCoords[] = [];
  zoom: number = 15;
  defaultlat: number;
  defaultlng: number;

  drilledDownUser: User;
  mapDetails: MapModel;
  userAddress: string;
  hashedSurbordinateId: string;
  dateTime: any;
  record = true;
  addressHistory: string[] = [];
  branchMarkers: BranchCoords[] = [];
  branchNames: string[] = [];

  iconUrl = {
    url: 'http://maps.google.com/mapfiles/ms/icons/POI.png',
    scaledSize: {
      width: 25,
      height: 25
    }
  };

  mainIconUrl = {
    url: 'https://drive.google.com/open?id=1hhoDfmEUy9L5Fa7Zg4LHp9Rb0LkGs78y',
    scaledSize: {
      width: 35,
      height: 35
    }
  };

  branchIconUrl = {
    url: 'http://maps.google.com/mapfiles/ms/icons/rangerstation.png',
    scaledSize: {
      width: 25,
      height: 25
    }
  };

  constructor(private userService: UserDetailService,
    private profileMapService: ProfileMapService,
    private mapLoader: MapsAPILoader,
    private staffService: StaffService,
    private ngZone: NgZone,
    private branchService: BranchLocationsService) {
    this.trackHistoryFilter = this.createFormGroup();
  }
  trackHistoryFilter: FormGroup;

  minDate = new Date(2019, 0, 1);
  maxDate = new Date(2020, 0, 1);


  ngOnInit() {
    this.branchService.fetchBranches();
    this.branchService.getBranches();
    this.branchService.branchNames.subscribe(res => {
      res.forEach(name => this.branchNames.push(name));
    });
    this.branchService.branchCoords.subscribe(res => {
      res.forEach(coord => {
        this.branchMarkers.push({
          lat: coord.lat,
          long: coord.long
        });
      });
    });
    this.getUser();
    this.getUserLocation();
    this.staffService.userHashedStaffId.subscribe(hashedId => {
      this.hashedSurbordinateId = hashedId;
    });

    setTimeout(() => {
      this.getStaffLastLocation().then(onsuccess => {

        // Subscribe to map service to listen for location changes
        this.profileMapService.staffLastLocation.subscribe((lastLocation) => {

          this.profileMapService.dateTime.subscribe(res => this.dateTime = res);
          this.mapDetails = {};
          this.pushMapDetails(lastLocation[0]);
        });
      },
        onerror => {
        }).catch((error) => {
        });
    }, 1500);
  }

  getUser() {
    this.userService.user.subscribe((res) => {
      this.drilledDownUser = res;
    });
  }

  getStaffLastLocation() {
    return new Promise((resolve, reject) => {
      const userId = this.hashedSurbordinateId;
      this.profileMapService.getStaffLocations(userId).then(result => {
        // Push filtered result for rendering
        this.profileMapService.dateTime.subscribe(res => this.dateTime = res);
        this.pushMapDetails(result[0]);
        console.log(result[0]);
        resolve(result);
      },
        (onerror) => {
          reject(null);
        });

    });

  }

  pushMapDetails(item) {

    this.mapDetails = (
      {
        id: this.dateTime,
        coords: {
          latitude: +item.coords.latitude,
          longitude: +item.coords.longitude,
          accuracy: item.coords.accuracy,
          altitude: item.coords.altitude,
          heading: item.coords.heading,
          speed: item.coords.speed
        },
        user_info: {
          device_name: item.user_info.device_name,
          device_model: item.user_info.device_model,
          device_os: item.user_info.device_os,
          device_os_version: item.user_info.device_os_version,
          provider: item.user_info.provider,
          staff_name: item.user_info.staff_name,
          staff_id: item.user_info.staff_id
        },
        timestamp: item.timestamp,
        mock: item.mock,
        provider: {
          enabled: item.provider.enabled,
          gps: item.provider.gps,
          network: item.provider.network,
          status: item.provider.status
        }
      }
    );

    this.getUserAddress(item.coords.latitude, item.coords.longitude);
  }

  getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          this.defaultlat = position.coords.latitude; // Works fine
          this.defaultlng = position.coords.longitude;  // Works fine
        },
        error => {
        }
      );
    }
  }

  getUserAddress(lat, lng) {
    let geocoder = new google.maps.Geocoder();
    const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0] != null) {
          this.ngZone.run(() => {
            this.userAddress = results[0].formatted_address;
          });
        } else {
        }
      }
    });
  }

  createFormGroup() {
    return new FormGroup({
      selectDate: new FormControl('', Validators.required),
      startTime: new FormControl('00:00', Validators.required),
      endTime: new FormControl('23:59', Validators.required)
    });
  }


  onSearch(form) {
    this.trackHistoryFilter.reset();
    this.trackHistoryFilter.get('startTime').patchValue('00:00');
    this.trackHistoryFilter.get('endTime').patchValue('23:59');

    this.trackHistoryCoords = [];

    setTimeout(() => {
      const formDate = this.convert(form.selectDate);
      // '20-02-2019, 18:18:01'
      const startDate = formDate + ', ' + form.startTime + ':00';
      const endDate = formDate + ', ' + form.endTime + ':00';

      this.profileMapService.allOfOneStaffLocation.subscribe(
        res => {
          this.filterDate(res, startDate, endDate);
        }
      );
    }, 1000);

  }

  tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }

  convert(str) {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join('-');
  }

  filterDate(object: any, startDate: string, endDate: string) {
    const filteredResults = [];
    for (const key in object) {
      if (object.hasOwnProperty(key)) {

        const rangeMoment = extendMoment(moment);
        if (key != 'id') {
          const range = rangeMoment.range(this.convertDate(startDate), this.convertDate(endDate));
          if (range.contains(this.convertDate(key))) {

            filteredResults.push(object[key]);

          } else {
          }

        }
      }
    }
    if (filteredResults.length > 0) {
      filteredResults.forEach(element => {
        this.pushTrackHistoryCoords(+element.coords.latitude, +element.coords.longitude, element.timestamp);
      });
      this.record = true;
    } else if (filteredResults.length === 0) {
      this.record = false;
    }

    this.defaultlat = this.trackHistoryCoords[this.trackHistoryCoords.length - 1].lat;
    this.defaultlng = this.trackHistoryCoords[this.trackHistoryCoords.length - 1].lng;
  }

  convertDate(dateString: string): Date {

    const newDate = dateString.split(',');
    const datePart = newDate[0];
    const timePartArray = newDate[1].trim().split(':');
    console.log('');
    const formattedDate = datePart.split('-');
    const day = parseInt(formattedDate[2], 10);
    const month = parseInt(formattedDate[1], 10);
    const year = parseInt(formattedDate[0], 10);

    return new Date(year, month, day,
      parseInt(timePartArray[0], 10),
      parseInt(timePartArray[1], 10),
      parseInt(timePartArray[2], 10), 0);

  }

  pushTrackHistoryCoords(lat, lng, timeStamp) {

    this.trackHistoryCoords.push(
      {
        lat: lat,
        lng: lng,
        timeStamp: timeStamp
      }
    );

    this.getAddressHistory(lat, lng);
  }


  // this.trackHistoryCoords.forEach(
  //   element => {
  //     this.getAddressHistory(element.lat, element.lng);
  //   }
  // );


  getAddressHistory(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0] != null) {
          this.ngZone.run(() => {
            console.log('formatted address - ', results[0].formatted_address);
            // this.add.push(results[0].formatted_address);
            // console.log(this.add);
          });
        } else {
        }
      }
    });
  }

  // GetAllUserAddress() {
  //   var timeOutSeconds = 1;
  //   console.log('all markers', this.markers);

  //   this.markers.forEach(latlng => {

  //     setTimeout(() => {
  //       this.getUserAddress(latlng.lat, latlng.lng);

  //     }, 1500 * timeOutSeconds);
  //   });
  //   timeOutSeconds++;
  // }

  onMouseOver(infoWindow, $event: MouseEvent) {
    infoWindow.open();
  }

  onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
  }
}


interface TrackHistoryCoords {
  lat: number;
  lng: number;
  timeStamp: string;
}

