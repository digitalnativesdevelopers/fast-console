import { Component, OnInit } from '@angular/core';
import { UserDetailService } from 'src/app/console-application/core/user-detail.service';
import { User } from 'src/app/console-application/core/user-detail.model';
import { ProfileMapService } from '../profile-page/profile-map.service';
import { MapModel, battery } from '../../map-model';
import { StaffService } from '../staff.service';

@Component({
  selector: 'app-profile-sidebar',
  templateUrl: './profile-sidebar.component.html',
  styleUrls: ['./profile-sidebar.component.css']
})
export class ProfileSidebarComponent implements OnInit {
  drilledDownUser: User;
  mapDetails: MapModel;
  hashedSurbordinateId: string;
  constructor(private userService: UserDetailService,
    private profileMapService: ProfileMapService,
    private staffService: StaffService
  ) { }

  ngOnInit() {
    this.getUser();


    setTimeout(() => {
      this.staffService.userHashedStaffId.subscribe(hashedId => {
        this.hashedSurbordinateId = hashedId;
      });
      this.getStaffLastLocation().then(onsuccess => {
      },
      onerror => {
      });

    }, 1500);
  }

  getUser() {
    this.userService.user.subscribe((res) => {
      this.drilledDownUser = res;
    });
  }

  getStaffLastLocation() {
    return new Promise((resolve, reject) => {
      const userId = this.hashedSurbordinateId;
      this.profileMapService.getStaffLocations(userId).then(result => {
        // Push filtered result for rendering
        this.getSideBarDetails(result[0]);
        resolve(result);
      },
        (onerror) => {
          reject(null);
        });
    });
  }

  getSideBarDetails(item) {
    this.mapDetails = (
      {
        id: '',
        coords: {
          latitude: +item.coords.latitude,
          longitude: +item.coords.longitude,
          accuracy: item.coords.accuracy,
          altitude: item.coords.altitude,
          heading: item.coords.heading,
          speed: item.coords.speed
        },
        battery: {
          is_charging: item.battery.is_charging,
          level: this.formatMapDetails(item.battery),
        },
        odometer: item.odometer,
        user_info: {
          device_name: item.user_info.device_name,
          device_model: item.user_info.device_model,
          device_os: item.user_info.device_os,
          device_os_version: item.user_info.device_os_version,
          provider: item.user_info.provider,
          staff_name: item.user_info.staff_name,
          staff_id: item.user_info.staff_id
        },
      }
    );

  }

  formatMapDetails(battery: battery) {
    const batteryLevel = battery.level.toString();
    const result = batteryLevel.split('.');
    return result[1];
  }
}
