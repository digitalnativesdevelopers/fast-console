import { UserDetail } from './../../core/user-detail.model';
import { UserDetailService } from './../../core/user-detail.service';
import { TeamDetail } from './../team-details/team-detail.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeamDetailService } from '../team-details/team-detail-service.service';
import jsSHA from 'jssha';
import { ProfileMapService } from './profile-page/profile-map.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subject, BehaviorSubject } from 'rxjs';
import { StaffService } from './staff.service';
import { reject } from 'q';
// import { resolve } from 'dns';

@Component({
  selector: 'app-staff-page',
  templateUrl: './staff-page.component.html',
  styleUrls: ['./staff-page.component.css']
})
export class StaffPageComponent implements OnInit {
  id: any;
  team: TeamDetail[];
  filteredUser: UserDetail;
  staffId: string;
  hashedStaffId: Subject<string> = new BehaviorSubject<string>('');
  constructor(private route: ActivatedRoute, private teamService: TeamDetailService, private userService: UserDetailService,
    private profileMapService: ProfileMapService,
    private staffService: StaffService) { }

    ngOnInit() {
     this.getTeam();
  }


  private getSelectedUserDetail() {
    this.route.params.subscribe(params => {
      const id = +params['id']; // (+) converts string 'id' to a number
      const teamMember = this.team.find(x => x.position === id);
      this.userService.getUserDetails(teamMember.staffId);
      this.staffId = teamMember.staffId;
      this.staffService.HashUserStaffId(this.staffId);
    });
  }

   getTeam() {
    this.teamService.teamMembers.subscribe((team) => {
      this.team = team;
      this.getSelectedUserDetail();
    });
  }

}
