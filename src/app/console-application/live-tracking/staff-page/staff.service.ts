import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import jsSHA from 'jssha';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  userHashedStaffId: Subject<string> = new BehaviorSubject<string>('');
  supervisorhashedStaffId: Subject<string> = new BehaviorSubject<string>('');
  staffId: Subject<string> = new BehaviorSubject<string>('');

  constructor() { }

  getUserStaffId() {
    let result = this.staffId.subscribe(res =>res);
    this.HashUserStaffId(result);
  }

  getSupervisorStaffId() {
    // var result = this.staffId.subscribe(res => {return res});
    // this.HashSupervisorStaffId(result);
  }

  HashUserStaffId(result) {

    const algorithm = 'SHA-512';
    let hashedStaffId = this.Hash(algorithm, result);
    this.userHashedStaffId.next(hashedStaffId);
  }

  HashSupervisorStaffId(result) {
    const algorithm = 'SHA-512';

    let hashedStaffId = this.Hash(algorithm, result);
    this.supervisorhashedStaffId.next(hashedStaffId);
  }

  Hash(algorithm: string, data: string) {
    if (!data) { return; }
    const shaObj = new jsSHA(algorithm, 'TEXT');
    shaObj.update(data);
    return shaObj.getHash('HEX');
  }
}
