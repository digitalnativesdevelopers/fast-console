import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { MapModel } from '../map-model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
//import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class MapService {
  staffLocationCollection: AngularFirestoreCollection<MapModel[]>;
  staffLocationDoc: AngularFirestoreDocument<MapModel[]>;
  stafflocations: Observable<MapModel[]>;

  staffLastLocation: Subject<MapModel[]> = new BehaviorSubject<MapModel[]>([]);
  locations: Subject<any[]> = new BehaviorSubject<any[]>([]);

  constructor(public firestore: AngularFirestore) { }

  fetchStaffLocation() {
    return new Promise((resolve) => {
      this.staffLocationCollection = this.firestore.collection('live-locations');
      resolve(this.stafflocations = this.staffLocationCollection.snapshotChanges().pipe(
        map(changes => changes.map(a => {
          const data = a.payload.doc.data() as MapModel;
          data.id = a.payload.doc.id;
          return data;
        }))
      ));
    });
  }

  getStaffLocations(hashedSupervisorId) {
    return new Promise((resolve, reject) => {
      const superId = hashedSupervisorId;
      
      // const superId = 'b7b30e4f9e1a9467f4b161091167d8294ad8974fc8321544e45cd52bd5c14a76d5eca04207d067c1eeb55547f5dd41fafb527fc71b9bb06687f575361721b623';
      let result: any[] = [];
      var teamMembersId: string[] = [];
      this.fetchStaffLocation().then(response => {
        const galaxyUsersRef = this.firestore.collection('galaxy-users').ref.where('superId', '==', superId).get();
        galaxyUsersRef.then(doc => {
          doc.forEach(docField => {

            teamMembersId.push(docField.data().userId);
          });
          this.stafflocations.subscribe(location => {
            var id: any[] = JSON.parse(JSON.stringify(teamMembersId));
            result = [];
            if (teamMembersId.length > 0) {
              this.staffLastLocation.next(null);
              teamMembersId.forEach(id => {
                let userLocation = location.filter(userId => userId.id == id);
                if (userLocation.length > 0) {
                  let resultSize = Object.values(userLocation[0]).length;
                  const staffLastLocation = Object.values(userLocation[0]).slice(resultSize - 2, resultSize);
                  this.staffLastLocation.next(staffLastLocation);
                  result.push(staffLastLocation);
                }
              });
              this.locations.next(result);
              resolve(result);
            }
            if (!result) {
              reject(null);
            }
          });
        },
          (ondocError) => {
            reject(null);
          });

      },
        (error) => { });

    });

  }

}
