import { Component, OnInit, NgZone } from '@angular/core';
import { User } from 'src/app/console-application/core/user-detail.model';
import { UserDetailService } from 'src/app/console-application/core/user-detail.service';
import { MapsAPILoader } from '@agm/core';

declare var google: any;
import { StaffService } from '../staff-page/staff.service';
import { MapModel } from '../map-model';
import { MapService } from './map.service';
import { IMarker } from '../i-marker';
import { BranchLocationsService, BranchCoords } from '../branch-locations.service';

@Component({
  selector: 'app-live-map',
  templateUrl: './live-map.component.html',
  styleUrls: ['./live-map.component.css']
})
export class LiveMapComponent implements OnInit {
  zoom: number = 10;
  defaultlat: number;
  defaultlng: number;

  drilledDownUser: User;
  mapDetails: MapModel;
  userAddress: string[] = [];
  hashedSupervisorId: string;
  markers: IMarker[] = [];
  staffNames: string[] = [];
  timeStamp: string[] = [];
  branchMarkers: BranchCoords[] = [];
  branchNames: string[] = [];

  mainIconUrl = {
    url: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
    scaledSize: {
      width: 35,
      height: 35
    }
  };

  branchIconUrl = {
    url: 'http://maps.google.com/mapfiles/ms/icons/rangerstation.png',
    scaledSize: {
      width: 25,
      height: 25
    }
  };

  constructor(private userService: UserDetailService,
    private mapService: MapService,
    private mapLoader: MapsAPILoader,
    private staffService: StaffService,
    private ngZone: NgZone,
    private branchService: BranchLocationsService,
    private userDetailService: UserDetailService) { }

  ngOnInit() {
    // Subscribe to get a list of all branches
    this.branchService.fetchBranches();
    this.branchService.getBranches();
    this.branchService.branchNames.subscribe(res => {
      res.forEach(name => this.branchNames.push(name));
    });
    this.branchService.branchCoords.subscribe(res => {
      res.forEach(coord => {
        this.branchMarkers.push({
          lat: coord.lat,
          long: coord.long
        });
      });
    });

    // Subscribe to get user details
    this.getUser();
    this.getUserLocation();
    this.userDetailService.supervisorhashedStaffId.subscribe(hashedId => {
      this.hashedSupervisorId = hashedId;
      console.log('id - ', hashedId);
    });

    this.getAllTeamMembersLastLocation().then(onsuccess => {
      // Subscribe to map service to listen for location changes
      this.mapService.locations.subscribe((lastLocation) => {
        const userLastLocations = Object.values(lastLocation);
        this.markers = [];
        userLastLocations.forEach(location => {
          this.pushMarkers(location[0]);
          this.staffNames.push(location[0].user_info.staff_name);
          this.timeStamp.push(location[0].timestamp);
          this.pushMapDetails(location[0]);
        });
        this.GetAllUserAddress();
        // console.log('markers', this.markers);
      });
    },
      onerror => {
      });
  }

  // Subscribe to user service to get user
  getUser() {
    this.userService.user.subscribe((res) => {
      this.drilledDownUser = res;
    });
  }

  // Subscribe to get the an array of all team members last known location
  getAllTeamMembersLastLocation() {
    return new Promise((resolve, reject) => {
      const userId = this.hashedSupervisorId;
      this.mapService.getStaffLocations(userId).then(locations => {
        const userLastLocations = Object.values(locations);
        userLastLocations.forEach(location => {

          this.pushMarkers(location[0]);
          this.pushMapDetails(location[0]);
          this.staffNames.push(location[0].user_info.staff_name);
          this.timeStamp.push(location[0].timestamp);
        });
        // console.log('markers', this.markers);
        // this.GetAllUserAddress();
        resolve(userLastLocations);
      },
        (onerror) => {
          reject(null);
        });
    });
  }

  // Push the map details into the map details array for all users
  pushMapDetails(item) {

    this.mapDetails = (
      {
        id: '',
        coords: {
          latitude: +item.coords.latitude,
          longitude: +item.coords.longitude,
          accuracy: item.coords.accuracy,
          altitude: item.coords.altitude,
          heading: item.coords.heading,
          speed: item.coords.speed
        },
        user_info: {
          device_name: item.user_info.device_name,
          device_model: item.user_info.device_model,
          device_os: item.user_info.device_os,
          device_os_version: item.user_info.device_os_version,
          provider: item.user_info.provider,
          staff_name: item.user_info.staff_name,
          staff_id: item.user_info.staff_id
        },
        timestamp: item.timestamp
      }
    );

    // this.getUserAddress(item.coords.latitude, item.coords.longitude);
  }

  // Get signed in users location
  getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          this.defaultlat = position.coords.latitude; // Works fine
          this.defaultlng = position.coords.longitude;  // Works fine
        },
        error => {
        }
      );
    }
  }

  // Call reverse geolocation API to get address from geo coordinates
  getUserAddress(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    var latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };

    geocoder.geocode({ 'location': latlng }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0] != null) {
          this.ngZone.run(() => {
            console.log('formatted address - ', results[0].formatted_address);
            this.userAddress.push(results[0].formatted_address);
          });
        } else {
          alert('No address available');
        }
      }
    });
  }

  // Push lat and lng values into the marker array
  pushMarkers(location: any) {
    if (!location) { return; }
    this.markers.push({
      lat: location.coords.latitude,
      lng: location.coords.longitude
    });
  }

  // Get address for all users location
  GetAllUserAddress() {
    var timeOutSeconds = 1;
    console.log('all markers', this.markers);

    this.markers.forEach(latlng => {

      setTimeout(() => {
        this.getUserAddress(latlng.lat, latlng.lng);

      }, 1500 * timeOutSeconds);
    });
    timeOutSeconds++;
  }

  onMouseOver(infoWindow, $event: MouseEvent) {
    infoWindow.open();
  }

  onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
  }
}

