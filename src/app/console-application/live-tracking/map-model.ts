export class MapModel {
    id?: string;
    activity?: activity;
    battery?: battery;
    user_info?: user_info;
    is_moving?: boolean;
    odometer?: number;
    timestamp?: string;
    uuid?: string;
    coords?: coords;
    provider?: provider;
    mock?: boolean;
}

interface coords {
    latitude: number;
    longitude: number;
    accuracy: string;
    heading: string;
    altitude: string;
    speed: string;
}

interface activity {
    confidence: number;
    type: number;
}

export interface battery {
    is_charging: string;
    level: string;
}

// tslint:disable-next-line: class-name
interface user_info {
    device_model: string;
    device_name: string;
    device_os: string;
    device_os_version: string;
    staff_id: string;
    provider: string;
    staff_name: string;
}

interface provider {
    enabled: string;
    gps: string;
    network: string;
    status: string;
}

