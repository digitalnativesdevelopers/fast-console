import { StaffPageComponent } from './staff-page/staff-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LiveTrackingComponent } from './live-tracking.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LiveMapComponent } from './live-map/live-map.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { RoleGuard } from 'src/app/Guard/role.guard';


const routes: Routes = [
  {path: '', component: LiveTrackingComponent,
    children: [
     {path: '', redirectTo: 'live-map', pathMatch: 'full'},
     // {path: 'dashboard', component: DashboardComponent },
      {path: 'live-map', component: LiveMapComponent, canActivate: [RoleGuard]},
      {path: 'team-details', component: TeamDetailsComponent, canActivate: [RoleGuard]},
      {path: 'staff/:id', component: StaffPageComponent}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LiveTrackingRoutingModule { }
