import { CoreModule } from './../core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiveTrackingRoutingModule } from './live-tracking-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { LiveMapComponent } from './live-map/live-map.component';
import { LiveTrackingComponent } from './live-tracking.component';
import { AmazingTimePickerModule } from "amazing-time-picker";
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StaffPageComponent } from './staff-page/staff-page.component';
import { ProfileSidebarComponent } from './staff-page/profile-sidebar/profile-sidebar.component';
import { ProfilePageComponent } from './staff-page/profile-page/profile-page.component';
import { AgmCoreModule } from '@agm/core';
import { AngularFireModule } from '@angular/fire';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    DashboardComponent, TeamDetailsComponent, LiveMapComponent, LiveTrackingComponent,
    StaffPageComponent, ProfileSidebarComponent, ProfilePageComponent
  ],

  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    LiveTrackingRoutingModule,
    NgxMaterialTimepickerModule.forRoot(),
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    AgmCoreModule,
    AngularFireModule,
    AmazingTimePickerModule,
    CoreModule
  ],
  exports: [
    LiveTrackingComponent,
    StaffPageComponent
  ]
})
export class LiveTrackingModule { }
