import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsoleApplicationComponent } from './console-application.component';

describe('ConsoleApplicationComponent', () => {
  let component: ConsoleApplicationComponent;
  let fixture: ComponentFixture<ConsoleApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsoleApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsoleApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
