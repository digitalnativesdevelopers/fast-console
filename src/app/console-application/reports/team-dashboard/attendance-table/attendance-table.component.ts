import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AttendanceTableService } from './staff-attendance/attendance-table.service';

export interface StaffElement {
  staffId: string;
  name: string;
  date: string;
  clockin: string;
  clockout: string;
}

const STAFF_DATA: StaffElement[] = [
  {staffId: 'P7558', name: 'Olaosebikan Sulaiman', date: '21/02/2019', clockin: '08:00 AM', clockout: '05:00 PM'},
  {staffId: 'P7558', name: 'Olaosebikan Sulaiman', date: '21/02/2019', clockin: '08:00 AM', clockout: '05:00 PM'},
];

@Component({
  selector: 'app-attendance-table',
  templateUrl: './attendance-table.component.html',
  styleUrls: ['./attendance-table.component.css']
})
export class AttendanceTableComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'date', 'clockin', 'clockout'];
  // dataSource = new MatTableDataSource(STAFF_DATA);
  dataSource: MatTableDataSource<any>;

  dateSearch: FormGroup;

  search = false;
  public start: string;
  public end: string;
  selectedView = '';

  public currentDay: string = new Date().toString().slice(0, 15);
  public today = this.convertDate(new Date);
  public branchName = this.route.snapshot.params['branchName'];
  public totalCount: number;
  public noOfStaffInBranch: number;
  public totalNumberOfAbsentStaff: number;
  public percentage: number;
  public lateClicked = false;
  public earlyClicked = false;
  public absentClicked = false;
  public earlyClockOutClicked = false;
  public clockedInClicked = false;
  public showAllClicked = true;
  public loading: boolean;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(  private router: Router ,private route: ActivatedRoute, private attendanceService: AttendanceTableService ) {
    this.dateSearch = this.createFormGroupAllStaff();
  }

  getAttendanceData() {
    const attendance = [];
    let clockOutTime: string;
    let clockInTime: string;
    let date: string;
    this.attendanceService.teamAttendance$.subscribe(res => {
      if (res) {
        for (const staffRecord of res) {
          if ( staffRecord.timeOut === null ) {
            clockOutTime = '';
          } else {
            clockOutTime = staffRecord.timeOut;
          }
          if (staffRecord.timeIn === null ) {
            clockInTime = '';
          } else {
            clockInTime = staffRecord.timeIn;
          }
          if ( staffRecord.clockInDate === null) {
            date = '';
          } else if (staffRecord.dateAbsent){
            date = staffRecord.dateAbsent;
          } else {
            date = staffRecord.clockInDate;
          }
          attendance.push(
            { 'clockOut': clockOutTime,
              'clockIn': clockInTime,
              'date': date,
              'staffId': staffRecord.staffId,
              'staffName': staffRecord.staffName
            }
          );
        }
        this.attendanceService.loader$.subscribe( state => {
          console.log(state);
          this.loading = state;
        })
        this.dataSource = new MatTableDataSource(attendance);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
    this.attendanceService.teamStats$.subscribe(res => {
      if (res) {
        this.totalCount = res[0].totalCount || 0;
        this.noOfStaffInBranch = res[0].noOfStaffInBranch;
        this.totalNumberOfAbsentStaff = res[0].totalNumberOfAbsentStaff || 0;
        this.percentage = res[0].percentageOfTotal || 0;
      }
    });
  }

  navigate(staffId, staffName) {
    this.router.navigate(['/reports/staff-attendance', `${staffName},${staffId},${this.branchName}`]);
  }

  showAll() {
    this.attendanceService._getTeamAttendance({
      'branchName': this.branchName,
      'startDate': this.today,
      'endDate': this.today,
      'status': 0
    });
    this.getAttendanceData();
  }

  ngOnInit() {
    this.attendanceService._getTeamAttendance({
      'branchName': this.branchName,
      'startDate': this.today,
      'endDate': this.today,
      'status': 0
    });
    this.getAttendanceData();
  }

  createFormGroupAllStaff() {
    return new FormGroup({
      startDate: new FormControl((new Date()).toISOString(), Validators.required),
      endDate: new FormControl((new Date()).toISOString(), Validators.required),
      filterOption: new FormControl('All', Validators.required)
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onSubmit(searchQuery) {
    let searchParam;
    let status: number;
    this.search = true;
    const formValue = this.dateSearch.value;
    this.start = formValue.startDate;
    this.end = formValue.endDate;
    let selectedView: string = this.dateSearch.controls['filterOption'].value;
    if ( searchQuery.filterOption === 'clockedIn'){
      status = 6;
      this.clockedInClicked = true;
      this.showAllClicked = false;
      this.lateClicked = false;
      this.earlyClicked = false;
      this.earlyClockOutClicked = false;
      this.absentClicked = false;
    } else if ( searchQuery.filterOption === 'late') {
      status = 2;
      this.showAllClicked = false;
      this.lateClicked = true;
      this.clockedInClicked = false;
      this.showAllClicked = false;
      this.earlyClicked = false;
      this.earlyClockOutClicked = false;
      this.absentClicked = false;
    } else if ( searchQuery.filterOption === 'Early') {
      status = 1;
      this.showAllClicked = false;
      this.earlyClicked = true;
      this.clockedInClicked = false;
      this.showAllClicked = false;
      this.lateClicked = false;
      this.earlyClockOutClicked = false;
      this.absentClicked = false;
    } else if ( searchQuery.filterOption === 'Early Clock-Out') {
      status = 3;
      this.showAllClicked = false;
      this.earlyClockOutClicked = true;
      this.clockedInClicked = false;
      this.showAllClicked = false;
      this.lateClicked = false;
      this.earlyClicked = false;
      this.absentClicked = false;
    } else if (searchQuery.filterOption === 'Absent') {
      status = 4;
      this.showAllClicked = false;
      this.absentClicked = true;
      this.clockedInClicked = false;
      this.showAllClicked = false;
      this.lateClicked = false;
      this.earlyClicked = false;
      this.earlyClockOutClicked = false;
    }
    searchParam = {
      'branchName': this.route.snapshot.params['branchName'],
      'startDate': this.convertDate(searchQuery.startDate),
      'endDate': this.convertDate(searchQuery.endDate),
      'status': status
    };
    this.attendanceService._getTeamAttendance(searchParam);
    this.getAttendanceData();
    this.dateSearch.reset;
  }

  convertDate(str) {
    const date = new Date(str),
    mnth = ('0' + (date.getMonth() + 1)).slice(-2),
    day = ('0' + date.getDate()).slice(-2);
    return [ mnth, day, date.getFullYear()].join('/');
  }
  
}
