export interface AttendanceData {
    position: number;
    name: string;
    staffid: string;
    clockin: string;
    clockout: string;
}
