import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StaffAttendanceService {

  private staffRecord = new BehaviorSubject<any>(null);
  staffRecord$ = this.staffRecord.asObservable();

  private staffStat = new BehaviorSubject<any>(null);
  staffStat$ = this.staffStat.asObservable();

  constructor(private http: HttpClient) { 

  }

  getStaffAttendanceRecord(requestParam): Observable<any> {
    return this.http.post(environment.fastconsole_getStaffAttendanceRecord, requestParam)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  _getStaffAttendanceRecord(requestParam) {
    this.staffRecord.next(null);
    this.staffStat.next(null);
    const stat = [];
    this.getStaffAttendanceRecord(requestParam).subscribe(res => {
      stat.push({
        'absentCount': res.data.noOfTimesAbsent,
        'lateCount': res.data.noOfTimesLate,
        'clockInCount': res.data.totalClockinCount,
        'latenessPercentage': res.data.dateRangePercentageLateness,
      });
      this.staffStat.next(stat);
      this.staffRecord.next(res.data.singleStaffAttendance);
    });
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
