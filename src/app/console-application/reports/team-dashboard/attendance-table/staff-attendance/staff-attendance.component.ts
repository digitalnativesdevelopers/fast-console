import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StaffAttendanceService } from './staff-attendance.service';


export interface StaffElement {
  // position: number;
  // name: string;
  date: string;
  timeIn: string;
  timeOut: string;
}

const STAFF_DATA: StaffElement[] = [
  {date: '21/02/2019', timeIn: '08:00 AM', timeOut: '05:00 PM'},
  {date: '21/02/2019', timeIn: '08:00 AM', timeOut: '05:00 PM'},
];

@Component({
  selector: 'app-staff-attendance',
  templateUrl: './staff-attendance.component.html',
  styleUrls: ['./staff-attendance.component.css']
})
export class StaffAttendanceComponent implements OnInit {

  displayedColumns: string[] = ['date', 'clockin', 'clockout'];
  dataSource: MatTableDataSource<any>;

  public latePercentage: number;
  public lateCount: number;
  public absentCount: number;
  public clockInCount: number;


  singleStaffForm: FormGroup;

  search = false;
  public start: string;
  public end: string;
  selectedView = '';

  public newDate = new Date();
  public currentDay: string = new Date().toString().slice(0, 15);
  public sevenDaysDisplay = new Date(this.newDate.getTime() - (7 * 24 * 60 * 60 * 1000)).toString().slice(0, 15);   // seven days before for the template


  public staffName: string;
  public staffId: string;
  public branchName: string;
  public today = this.convertDate(new Date);
  public sevenDaysBefore = this.convertDate(new Date(this.newDate.getTime() - (7 * 24 * 60 * 60 * 1000)));
  public sevenDays = new Date(this.newDate.getTime() - (7 * 24 * 60 * 60 * 1000));  // used to set default for startdate form, not stringified


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor( private route: ActivatedRoute, private staffAttendanceService: StaffAttendanceService, private location: Location) 
  { 
    this.singleStaffForm = this.createFormGroupSingleStaff();
  }

  getStaffAttendanceRecord() {
    const attendance = [];
    let timeIn: string;
    let timeOut: string;
    this.staffAttendanceService.staffRecord$.subscribe(res => {
      if (res) {
        for (const staffRecord of res) {
          if (staffRecord.timeOut){
            timeOut = staffRecord.timeOut;
          } else {timeOut = ''}
          if (staffRecord.timeIn) {
            timeIn = staffRecord.timeIn;
          } else { timeOut = staffRecord.timeOut }
          attendance.push(
            { 'timeOut': timeOut,
              'timeIn': timeIn,
              'date': staffRecord.clockInDate
            }
          );
        }
        this.dataSource = new MatTableDataSource(attendance);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });

    this.staffAttendanceService.staffStat$.subscribe(res => {
      if (res) {
        this.latePercentage = res[0].latenessPercentage;
        this.lateCount = res[0].lateCount;
        this.clockInCount = res[0].clockInCount;
        this.absentCount = res[0].absentCount;
      }
    });
  }

  ngOnInit() {
    this.staffId = this.route.snapshot.params['staffDetails'].split(',')[1];
    this.staffName = this.route.snapshot.params['staffDetails'].split(',')[0];
    this.branchName = this.route.snapshot.params['staffDetails'].split(',')[2];
    this.staffAttendanceService._getStaffAttendanceRecord({
      'branchName': this.branchName,
      'startDate': this.sevenDaysBefore,
      'endDate': this.today,
      'staffId': this.staffId
    });
    this.getStaffAttendanceRecord();
  }

  createFormGroupSingleStaff() {
    return new FormGroup({
      startDate: new FormControl(this.sevenDays.toISOString(), Validators.required),
      endDate: new FormControl((new Date()).toISOString(), Validators.required),
     // filterOption: new FormControl('', Validators.required)
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onSubmit() {
    this.search = true;
    const formValue = this.singleStaffForm.value;
    this.start = this.convertDate(formValue.startDate);
    this.end = this.convertDate(formValue.endDate);
    // let selectedView: string = this.singleStaffForm.controls['filterOption'].value;
    const requestParam = {
      'branchName': this.branchName,
      'startDate': this.start,
      'endDate': this.end,
      'staffId': this.staffId
    };
    this.staffAttendanceService._getStaffAttendanceRecord(requestParam);
    this.getStaffAttendanceRecord();
    this.singleStaffForm.reset;
  }

  goBack() {
    this.location.back();
  }

  convertDate(str) {
    const date = new Date(str),
    mnth = ('0' + (date.getMonth() + 1)).slice(-2),
    day = ('0' + date.getDate()).slice(-2);
    return [ mnth, day, date.getFullYear()].join('/');
  }
}
