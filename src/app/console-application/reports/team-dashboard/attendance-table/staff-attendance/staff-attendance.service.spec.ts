import { TestBed } from '@angular/core/testing';

import { StaffAttendanceService } from './staff-attendance.service';

describe('StaffAttendanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StaffAttendanceService = TestBed.get(StaffAttendanceService);
    expect(service).toBeTruthy();
  });
});
