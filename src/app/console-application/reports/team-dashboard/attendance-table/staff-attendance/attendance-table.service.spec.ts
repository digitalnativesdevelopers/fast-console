import { TestBed } from '@angular/core/testing';

import { AttendanceTableService } from './attendance-table.service';

describe('AttendanceTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AttendanceTableService = TestBed.get(AttendanceTableService);
    expect(service).toBeTruthy();
  });
});
