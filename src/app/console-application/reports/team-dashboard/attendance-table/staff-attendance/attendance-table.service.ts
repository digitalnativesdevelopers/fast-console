import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AttendanceTableService {
  private teamAttendance = new BehaviorSubject<any>(null);
  teamAttendance$ = this.teamAttendance.asObservable();

  private teamStats = new BehaviorSubject<any>(null);
  teamStats$ = this.teamStats.asObservable();

  private loader = new BehaviorSubject<any>(null);
  loader$ = this.loader.asObservable();

  constructor( private http: HttpClient ) { }

  getTeamAttendance(requestParam): Observable<any> {
    return this.http.post(environment.fastconsole_getTeamAttendanceLog, requestParam)
    .pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  _getTeamAttendance(requestParam) {
    this.updateLoader(true);
    this.teamAttendance.next(null);
    this.teamStats.next(null);
    const stats = [];
    this.getTeamAttendance(requestParam).subscribe(res => {
      if ( res.code = '00' ) {
        stats.push({
          'noOfStaffInBranch': res.data.noOfStaffInBranch,
          'totalCount': res.data.totalNumberOfClockedInStaff || res.data.totalCount,
          'percentageOfTotal': res.data.percentagebyDateRange,
          'totalNumberOfAbsentStaff' : res.data.totalNumberOfAbsentStaff || res.data.totalCount
        });
        this.teamStats.next(stats);
        if (res.data.allStaff) {
          this.teamAttendance.next(res.data.allStaff);
        } else if (res.data.attendanceDetails) {
          this.teamAttendance.next(res.data.attendanceDetails);
        } else {
          this.teamAttendance.next(res.data.staffNotClockedIn);
        }
        this.updateLoader(false);
      }
    }, (err: HttpErrorResponse) => {
      this.updateLoader(true);
      console.log(err);
    });
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  updateLoader(state) {
    this.loader.next(state);
  }
}
