import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-team-dashboard',
  templateUrl: './team-dashboard.component.html',
  styleUrls: ['./team-dashboard.component.css']
})
export class TeamDashboardComponent implements OnInit {
  public teamName: string;
  constructor( private route: ActivatedRoute ) { }

  ngOnInit() {
    this.teamName = this.route.snapshot.params['branchName'];
  }

}