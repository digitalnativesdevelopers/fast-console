import { Component, OnInit } from '@angular/core';
import { TeamService } from './team.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  public teams: [];
  public loading: boolean;
  constructor( private teamService: TeamService, private route: Router) { }

  getSupervisorTeams() {
    this.teamService.team$.subscribe(response => {
      this.teamService.loader$.subscribe( state => {
        this.loading = state;
      });
      this.teams = response;
    });
  }

  navigate(branchName) {
   this.route.navigate(['/reports/team-dashboard', branchName]);
  }
  search(val) {
    console.log(val);
  }
  ngOnInit() {
    this.teamService._getSupervisorTeams();
    this.getSupervisorTeams();
  }

}
