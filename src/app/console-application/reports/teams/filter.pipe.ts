import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(teams: any, searchTerm: any): any {
    if (searchTerm === null) {
      return teams;
    } else {
      return teams.filter(team => {
        // tslint:disable-next-line:max-line-length
        return team.branchName.toLowerCase().includes(searchTerm.toLowerCase()) || team.directorateName.toLowerCase().includes(searchTerm.toLowerCase()) || team.directorateCode.toLowerCase().includes(searchTerm.toLowerCase());
      });
    }
  }

}
