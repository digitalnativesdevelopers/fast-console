import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teams = new BehaviorSubject<any>(null);
  team$ = this.teams.asObservable();

  private loader = new BehaviorSubject<any>(null);
  loader$ = this.loader.asObservable();

  constructor(private http: HttpClient) { }

  getSupervisorTeams (): Observable<any> {
    return this.http.get(`${environment.fastconsole_getSupervisorTeams}?staffId=p6124`)
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

  _getSupervisorTeams () {
    this.updateLoader(true);
    this.getSupervisorTeams().subscribe(res => {
      if (res.code = '00') {
        this.teams.next(res.data);
        this.updateLoader(false);
      }
    },
    (err: HttpErrorResponse) => {
      this.updateLoader(true);
      console.log(err);
    }
    );
  }

  handleError( error: HttpErrorResponse ) {
    return throwError(error);
  }

  updateLoader(state) {
    this.loader.next(state);
  }
}
