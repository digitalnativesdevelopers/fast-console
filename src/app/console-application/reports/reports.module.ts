import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { TeamsComponent } from './teams/teams.component';
import { TeamDashboardComponent } from './team-dashboard/team-dashboard.component';
import { StatisticsComponent } from './team-dashboard/statistics/statistics.component';
import { AttendanceTableComponent } from './team-dashboard/attendance-table/attendance-table.component';
import { LiveTrackingModule } from '../live-tracking/live-tracking.module';
import { AmazingTimePickerModule } from "amazing-time-picker";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgCircleProgressModule } from 'ng-circle-progress';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { StaffAttendanceComponent } from './team-dashboard/attendance-table/staff-attendance/staff-attendance.component';
import { SharedModule } from '../../Shared/shared.module';
import { FilterPipe } from './teams/filter.pipe';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [ReportsComponent, TeamsComponent, TeamDashboardComponent, StatisticsComponent, AttendanceTableComponent, StaffAttendanceComponent, FilterPipe],
  imports: [
    CommonModule,
    SharedModule,
    ReportsRoutingModule, ReactiveFormsModule,
    AmazingTimePickerModule, FormsModule, NgxMaterialTimepickerModule,
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    SharedModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
  ]
})
export class ReportsModule { }
