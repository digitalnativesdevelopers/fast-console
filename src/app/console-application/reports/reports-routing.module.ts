import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { TeamsComponent } from './teams/teams.component';
import { TeamDashboardComponent } from './team-dashboard/team-dashboard.component';
import { StaffAttendanceComponent } from './team-dashboard/attendance-table/staff-attendance/staff-attendance.component';
import { RoleGuard } from 'src/app/Guard/role.guard';
import { ReportGuard } from '../../Guard/report.guard';

const routes: Routes = [
  {path: '', component: ReportsComponent,
    children: [
     {path: '', redirectTo: 'teams', pathMatch: 'full'},
     {path: 'teams',  component: TeamsComponent, canActivate: [ReportGuard]},
     {path: 'team-dashboard/:branchName', component: TeamDashboardComponent },
     {path: 'staff-attendance/:staffDetails', component: StaffAttendanceComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }

