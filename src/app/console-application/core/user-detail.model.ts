
export class User implements UserDetail {
  JobDescription: string;
  StaffName: string;
  Photo: string;
  LoginName: string;
  StaffId: string;
  constructor(
    JobDescription: string,
    StaffName: string,
    Photo: string,
    LoginName: string,
    StaffId: string
    ) {

      this.JobDescription = JobDescription;
      this.LoginName = LoginName;
      this.Photo = Photo;
      this.StaffName = StaffName;
      this.StaffId = StaffId;
    }
}

export interface UserDetail {
  JobDescription: string;
  StaffName: string;
  Photo: string;
  LoginName: string;
}
