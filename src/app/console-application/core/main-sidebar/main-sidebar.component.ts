import { Component, OnInit } from '@angular/core';
import { UserDetailService } from '../user-detail.service';
import { User } from '../user-detail.model';
import { MapService } from '../../live-tracking/live-map/map.service';
import { AuthenticationService } from 'src/app/auth/authentication.service';

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  styleUrls: ['./main-sidebar.component.css']
})
export class MainSidebarComponent implements OnInit {
  user: User;
  isAuthorized: boolean;
  constructor(private supervisorService: UserDetailService,
    private mapService: MapService, private auth: AuthenticationService) { }

  checkIsAuthorized(): any {
      this.auth.getAuthorization().then( success => {
      if ( this.auth.isAuthorized() === 'authorized and isTracking') {
        this.isAuthorized = true;
      } else if (this.auth.isAuthorized() === 'authorized and notTracking'){
        this.isAuthorized = true;
      } else if (this.auth.isAuthorized() === 'not authorized and not tracking') {
        this.isAuthorized = false;
      }
      console.log(this.isAuthorized);
   },
   err => {
     return false;
   });
  }

  ngOnInit() {
    this.supervisorService.supervisor.subscribe((user) => {
      this.user = user;
    });
    this.mapService.staffLastLocation.subscribe( res => {

    });
    this.checkIsAuthorized();
    
  }


}
