import { User } from './user-detail.model';
import { Injectable } from '@angular/core';

import { Subject, BehaviorSubject, Observable, from } from 'rxjs';
import { HttpClient,  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import jsSHA from 'jssha';



@Injectable({
  providedIn: 'root'
})
export class UserDetailService {


  supervisor: Subject<User> = new BehaviorSubject<User>(null);
  user: Subject<User> = new BehaviorSubject<User>(null);
  supervisorId: Subject<string> = new BehaviorSubject<string>(null);
  supervisorhashedStaffId: Subject<string> = new BehaviorSubject<string>('');
  constructor(private http: HttpClient) {}

  getSupervisorDetailsFromService(staffId: string): Observable<any> {
    const path = environment.fastconsole_getSupervisorDetails;
    return this.http
      .get<any>(path, {
        params: {
          staffId: staffId
        }
      })
      .pipe
      // retry(3),

      // catchError(this.handleError) // then handle the error
      ();
  }

  getSupervisorDetails(staffId: string) { // method called to get the current logged on user table
    this.getSupervisorDetailsFromService(staffId).subscribe((response) => {

      const algorithm = 'SHA-512';
      let hashedStaffId = this.Hash(algorithm, response.data.staffId);
      this.supervisorhashedStaffId.next(hashedStaffId);

       const supervisor =  this.extractUserData(response);

       this.supervisor.next(supervisor);
    });
  }

  getUserDetails(staffId: string) { // method called to get one user detail
    this.getSupervisorDetailsFromService(staffId).subscribe((response) => {
       const user =  this.extractUserData(response);
       this.user.next(user);
    });
  }
  extractUserData(response: any): User {
    let user = null;
    if (response.code === '00') {

        user = new User (
          response.data.jobDescription,
          response.data.staffName,
          'data:image/jpeg;base64, ' + response.data.photo,
          response.data.loginName,
          response.data.staffId
        );

    }
    return user;
  }

  Hash(algorithm: string, data: string) {
    if (!data) { return; }
    const shaObj = new jsSHA(algorithm, 'TEXT');
    shaObj.update(data);
    return shaObj.getHash('HEX');
  }
}
