import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainSidebarComponent } from './main-sidebar/main-sidebar.component';
import { RouterModule } from '@angular/router';

import { SafeHtml } from './Pipes/safe-html/safe-html.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { SubHeaderComponent } from './sub-header/sub-header.component';
@NgModule({
  declarations: [MainSidebarComponent, SafeHtml, MainHeaderComponent, SubHeaderComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    MainSidebarComponent,
    MainHeaderComponent,
    SubHeaderComponent,
    SafeHtml
  ]
})
export class CoreModule { }
