import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth/authentication.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {

  staffName: string;
  staffEmail: string;
  staffAvatar: string;
  staffAcronym: string;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {
    this.auth.getStaffDetails();
    this.getUserDetails();
  }

  getUserDetails () {
    this.auth.staffDetails$.subscribe( res => {
      if (res) {
        this.staffName = res.StaffName;
        this.staffAcronym = `${this.staffName.split(' ')[0][0]}${this.staffName.split(' ')[1][0]}`;
        this.staffAvatar = 'data:image/jpeg;base64,' + res.Avatar;
        this.staffEmail = res.Email;
      }
    });
  }

  // getUserRole() {
  //   this.auth.staffRole.subscribe( res => {
  //     if (res) {
  //       this.staffRole = window.atob(res);
  //       this.isSuperAdmin = this.AdminStatus;
  //     }
  //   });
  // }


}
