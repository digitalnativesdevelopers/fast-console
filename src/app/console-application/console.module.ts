import { ConsoleApplicationComponent } from './console-application.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsoleRoutingModule } from './console-routing.module';
import { LiveTrackingModule } from './live-tracking/live-tracking.module';
import { ReportsModule } from './reports/reports.module';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    ConsoleApplicationComponent,
  ],
  imports: [
    CommonModule,
    ConsoleRoutingModule,
    LiveTrackingModule,
    // ReportsModule
  ],
  exports: [
    ConsoleApplicationComponent
  ]
})
export class ConsoleModule { }
