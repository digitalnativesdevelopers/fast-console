import { TestBed } from '@angular/core/testing';

import { InterceptorConfigService } from './interceptor-config.service';

describe('InterceptorConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterceptorConfigService = TestBed.get(InterceptorConfigService);
    expect(service).toBeTruthy();
  });
});
