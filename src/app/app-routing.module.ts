import { TeamDetailsComponent } from './console-application/live-tracking/team-details/team-details.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { LiveTrackingComponent } from './console-application/live-tracking/live-tracking.component';
import { DashboardComponent } from './console-application/live-tracking/dashboard/dashboard.component';
import { LiveMapComponent } from './console-application/live-tracking/live-map/live-map.component';
import { StaffPageComponent } from './console-application/live-tracking/staff-page/staff-page.component';
import { RoleGuard } from './Guard/role.guard';
import { LimitedAccessComponent } from './limited-access/limited-access.component';
import { UnauthorizedPageComponent } from './unauthorized-page/unauthorized-page.component';


const routes: Routes = [
  { path: '', redirectTo: 'reports', pathMatch: 'full' },
  { path: 'reports', loadChildren: './console-application/reports/reports.module#ReportsModule'},
  { path: 'dashboard', component: DashboardComponent},
  {path: 'live-map', component: LiveMapComponent},
  {path: 'team-details', component: TeamDetailsComponent},
  {path: 'staff/:id', component: StaffPageComponent},
  { path: 'limited', component: LimitedAccessComponent },
  { path: 'no-access', component: UnauthorizedPageComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
