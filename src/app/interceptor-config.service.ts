import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorConfigService implements HttpInterceptor{

  constructor() { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req) {
      const duplicate = req.clone({ withCredentials : true});
      return next.handle(duplicate);
    }
    return next.handle(req);
  }
}
