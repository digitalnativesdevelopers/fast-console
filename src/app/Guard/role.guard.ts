import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private auth: AuthenticationService, private route: Router) {

  }
  canActivate( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const res = this.auth.getAuthorization().then(success => {
         if ( this.auth.isAuthorized() === 'authorized and isTracking') {
        console.log('authorized and tracking');
        return true;
      } else if (this.auth.isAuthorized() === 'authorized and notTracking'){
        console.log('authorized but not tracking');
        this.route.navigateByUrl('/reports');
        return false;
      } else if (this.auth.isAuthorized() === 'not authorized and not tracking') {
        this.route.navigateByUrl('/no-access');
        return false;
      }
      },
      err => {
        return false;
      });
     return  res;
  }
}
