import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsoleModule } from './console-application/console.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AgmCoreModule} from '@agm/core';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {LiveTrackingModule} from './../app/console-application/live-tracking/live-tracking.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { CoreModule } from './console-application/core/core.module';
import { InterceptorConfigService } from './utilities/interceptor-config.service';
import { LoaderComponent } from './Shared/loader/loader.component';
import { SharedModule } from './Shared/shared.module';
import { UnauthorizedPageComponent } from './unauthorized-page/unauthorized-page.component';
import { LimitedAccessComponent } from './limited-access/limited-access.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    UnauthorizedPageComponent,
    LimitedAccessComponent,
  ],
  imports: [
    BrowserModule,
    LiveTrackingModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    ConsoleModule,
    CoreModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxMaterialTimepickerModule.forRoot(),
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    CoreModule,
    AngularFireModule.initializeApp(environment.firebase, 'fastWebConsole'), // imports firebase/app needed for everything
    AngularFirestoreModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey
    }),
    AmazingTimePickerModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: InterceptorConfigService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {}
