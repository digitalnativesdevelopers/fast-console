
export const environment = {
  production: true,
  fastconsole_getSupervisorSubordinates: 'https://galaxy:4430/fastconsoleapi/api/fastusers/GetLineManagerSubordinates',
  fastconsole_getSupervisorDetails: 'https://galaxy:4430/fastconsoleapi/api/fastusers/GetStaffDetail',
  firebase: {
    apiKey: 'AIzaSyAbma1s4JGXCiLBLAJ_Cc91pEFJh6QaJIg',
    authDomain: 'galaxy-87adb.firebaseapp.com',
    databaseURL: 'https://galaxy-87adb.firebaseio.com',
    projectId: 'galaxy-87adb',
    storageBucket: 'galaxy-87adb.appspot.com',
    messagingSenderId: '477737672518'
  },
  googleMapsKey: 'AIzaSyBHAFSDagWxEqTt6W8Ieytw4Hc3K-E-iWY',

   // user details 
   galaxy_getUserDetails: 'https://galaxy:4430/GalaxyAPI/api/galaxy/GetUsersDetails',

   // Report endpoints

   fastconsole_getSupervisorTeams : 'https://galaxy:4430/FastConsoleApi/api/Branch/GetSupervisorBranch',
   fastconsole_getTeamAttendanceLog: 'https://galaxy:4430/FastConsoleApi/api/Attendance/GetAttendancePerBranch',
   fastconsole_getStaffAttendanceRecord: 'https://galaxy:4430/FastConsoleApi/api/Attendance/GetSingleStaffAttendanceLog',
   fastconsole_getStaffRole: 'https://galaxy:4430/FastConsoleApi/api/FastUsers/CheckSupervisorTrackingTeamStatus'
};
